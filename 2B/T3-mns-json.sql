--*****PLEASE ENTER YOUR DETAILS BELOW*****
--T3-mns-json.sql

--Student ID: 32071582
--Student Name: Anushka Reddy
--Unit Code: FIT3171
--Applied Class No: 04

/* Comments for your marker:

*/

/*3(a)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT TO GENERATE 
-- THE COLLECTION OF JSON DOCUMENTS HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer
SELECT JSON_OBJECT(
    '_id' VALUE a.appt_no,
    'datetime' VALUE TO_CHAR(a.appt_datetime, 'DD/MM/YYYY HH24:MI'),
    'provider_code' VALUE pr.provider_code,
    'provider_name' VALUE 
        pr.PROVIDER_TITLE || '. ' || 
        CASE 
            WHEN LTRIM(pr.PROVIDER_FNAME) IS NOT NULL THEN LTRIM(pr.PROVIDER_FNAME) || ' '
            ELSE ''
        END || 
        LTRIM(pr.PROVIDER_LNAME),
    'item_totalcost' VALUE SUM(i.item_stdcost * ai.as_item_quantity),
    'no_of_items' VALUE COUNT(DISTINCT ai.item_id),
    'items' VALUE JSON_ARRAYAGG(
        JSON_OBJECT(
            'id' VALUE ai.item_id,
            'desc' VALUE i.item_desc,
            'standardcost' VALUE i.item_stdcost,
            'quantity' VALUE ai.as_item_quantity
        )
    ) FORMAT JSON
) AS appointment_json
FROM mns.appointment a
JOIN mns.provider pr ON a.provider_code = pr.provider_code
JOIN mns.apptservice_item ai ON a.appt_no = ai.appt_no
JOIN mns.item i on ai.item_id = i.item_id
WHERE ai.as_item_quantity IS NOT NULL
GROUP BY a.appt_no, a.appt_datetime, pr.provider_code, pr.PROVIDER_TITLE, pr.PROVIDER_FNAME, pr.PROVIDER_LNAME
ORDER BY a.appt_no;

COMMIT;