--*****PLEASE ENTER YOUR DETAILS BELOW*****
--T2-mns-select.sql

--Student ID: 32071582
--Student Name: Anushka Reddy
--Unit Code: FIT3171
--Applied Class No: 04

/* Comments for your marker:
Many alias have been used to represent the temporary result set from a table
column, CTE, making it easier to reference its columns in the main query.
*/

/*2(a)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT FOR THIS PART HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer
SELECT item_id, item_desc, item_stdcost, item_stock
FROM mns.item
WHERE item_stock >= 50 AND LOWER(item_desc) LIKE '%composite%'
ORDER BY item_stock DESC, item_id;

/*2(b)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT FOR THIS PART HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer
SELECT 
    P.PROVIDER_CODE AS "Provider Code",
    P.PROVIDER_TITLE || '. ' || 
    CASE 
        WHEN LTRIM(P.PROVIDER_FNAME) IS NOT NULL THEN LTRIM(P.PROVIDER_FNAME) || ' '
        ELSE ''
    END || 
    LTRIM(P.PROVIDER_LNAME) AS "Provider Name"
FROM 
    mns.PROVIDER P
JOIN 
    mns.SPECIALISATION S ON P.SPEC_ID = S.SPEC_ID
WHERE 
    UPPER(S.SPEC_NAME) = 'PAEDIATRIC DENTISTRY'
ORDER BY
    P.PROVIDER_LNAME, 
    LTRIM(P.PROVIDER_FNAME), 
    P.PROVIDER_CODE;


    
/*2(c)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT FOR THIS PART HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer
SELECT service_code, service_desc, 
       LPAD(TO_CHAR(service_stdfee, '$999999.99'), 39) AS formatted_service_standard_fee
FROM mns.service
WHERE service_stdfee > (SELECT AVG(service_stdfee) FROM mns.service)
ORDER BY service_stdfee DESC, service_code;

/*2(d)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT FOR THIS PART HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer
SELECT 
    a.appt_no,
    a.appt_datetime,
    a.patient_no,
    TRIM(
        CASE WHEN pa.patient_fname IS NOT NULL THEN pa.patient_fname || ' ' ELSE '' END ||
        CASE WHEN pa.patient_lname IS NOT NULL THEN pa.patient_lname ELSE '' END
    ) AS patient_full_name,
    LPAD(TO_CHAR(NVL(sc.total_service_fee, 0) + NVL(ic.total_item_fee, 0), '$999999.99'), 43) AS formatted_appointment_total_cost
FROM 
    mns.appointment a
JOIN 
    mns.patient pa ON a.patient_no = pa.patient_no
LEFT OUTER JOIN 
    (
        SELECT 
            appt_no,
            SUM(apptserv_fee) AS total_service_fee
        FROM mns.appt_serv
        GROUP BY appt_no
    ) sc ON a.appt_no = sc.appt_no
LEFT OUTER JOIN 
    (
        SELECT 
            ai.appt_no,
            SUM(ai.as_item_quantity * i.item_stdcost) AS total_item_fee
        FROM mns.apptservice_item ai
        JOIN mns.item i ON ai.item_id = i.item_id
        GROUP BY ai.appt_no
    ) ic ON a.appt_no = ic.appt_no
WHERE 
    NVL(sc.total_service_fee, 0) + NVL(ic.total_item_fee, 0) = 
    (
        SELECT 
            MAX(NVL(sc2.total_service_fee, 0) + NVL(ic2.total_item_fee, 0))
        FROM 
            (
                SELECT 
                    appt_no,
                    SUM(apptserv_fee) AS total_service_fee
                FROM mns.appt_serv
                GROUP BY appt_no
            ) sc2
        FULL OUTER JOIN 
            (
                SELECT 
                    ai.appt_no,
                    SUM(ai.as_item_quantity * i.item_stdcost) AS total_item_fee
                FROM mns.apptservice_item ai
                JOIN mns.item i ON ai.item_id = i.item_id
                GROUP BY ai.appt_no
            ) ic2 ON sc2.appt_no = ic2.appt_no
    )
ORDER BY 
    a.appt_no;

/*2(e)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT FOR THIS PART HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer

SELECT 
    s.service_code,
    s.service_desc,
    s.service_stdfee AS standard_fee,
    ROUND(
        (
            SELECT AVG(aserv.apptserv_fee) 
            FROM mns.appt_serv aserv 
            WHERE aserv.service_code = s.service_code
            GROUP BY aserv.service_code
        ) - s.service_stdfee, 2
    ) AS service_fee_differential
FROM 
    mns.service s
WHERE 
    s.service_code IN 
    (
        SELECT distinct aserv.service_code
        FROM mns.appt_serv aserv
    )
ORDER BY 
    s.service_code;


/*2(f)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT FOR THIS PART HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer
SELECT 
    p.patient_no AS "PATIENT_NO",
    TRIM(
        CASE WHEN p.patient_fname IS NOT NULL THEN p.patient_fname || ' ' ELSE '' END ||
        CASE WHEN p.patient_lname IS NOT NULL THEN p.patient_lname ELSE '' END
    ) AS "PATIENTNAME",
    EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM p.patient_dob) AS "CURRENTAGE",
    COUNT(a.appt_no) AS "NUMAPPTS",
CASE 
    WHEN ROUND(100 * SUM(CASE WHEN a.appt_prior_apptno IS NOT NULL THEN 1 ELSE 0 END) / COUNT(a.appt_no), 1) = TRUNC(ROUND(100 * SUM(CASE WHEN a.appt_prior_apptno IS NOT NULL THEN 1 ELSE 0 END) / COUNT(a.appt_no), 1))
    THEN TO_CHAR(TRUNC(ROUND(100 * SUM(CASE WHEN a.appt_prior_apptno IS NOT NULL THEN 1 ELSE 0 END) / COUNT(a.appt_no), 1))) || '.0%'
    ELSE TO_CHAR(ROUND(100 * SUM(CASE WHEN a.appt_prior_apptno IS NOT NULL THEN 1 ELSE 0 END) / COUNT(a.appt_no), 1)) || '%'
END AS "FOLLOWUPS" FROM mns.patient p
LEFT JOIN mns.appointment a ON p.patient_no = a.patient_no
GROUP BY p.patient_no, p.patient_fname, p.patient_lname, p.patient_dob
ORDER BY p.patient_no;

/*2(g)*/
-- PLEASE PLACE REQUIRED SQL SELECT STATEMENT FOR THIS PART HERE
-- ENSURE that your query is formatted and has a semicolon
-- (;) at the end of this answer
SELECT
    LPAD(pr.provider_code, 8) AS "PCODE",
    LPAD(NVL(TO_CHAR(COUNT(a.appt_no)), '-'), 16) AS "NUMBERAPPTS",
    LPAD(CASE
                WHEN SUM(aserv.apptserv_fee) IS NULL THEN '-'
                ELSE '$' || TO_CHAR(TRUNC(SUM(aserv.apptserv_fee))) || 
                    CASE
                        WHEN MOD(SUM(aserv.apptserv_fee)*100,100) = 0 THEN '.00'
                        ELSE TO_CHAR(ROUND(MOD(SUM(aserv.apptserv_fee),1), 2), '0.99')
                    END
         END, 14) AS "TOTALFEES",
    LPAD(NVL(TO_CHAR(SUM(ai.as_item_quantity)), '-'), 10) AS "NOITEMS"
FROM 
    mns.provider pr
LEFT JOIN mns.appointment a ON pr.provider_code = a.provider_code AND a.appt_datetime BETWEEN TO_DATE('10-SEP-2023 09:00', 'DD-MON-YYYY HH24:MI') AND TO_DATE('14-SEP-2023 17:00', 'DD-MON-YYYY HH24:MI')
LEFT JOIN mns.appt_serv aserv ON a.appt_no = aserv.appt_no
LEFT JOIN mns.apptservice_item ai ON a.appt_no = ai.appt_no
GROUP BY pr.provider_code
ORDER BY pr.provider_code;


COMMIT;