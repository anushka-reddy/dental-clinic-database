--T4-mns-plsql.sql
--Student ID: 32071582
--Student Name: Anushka Reddy
--Unit Code: FIT3172
--Applied Class No: 04

/* Comments for your marker:

*/

SET SERVEROUTPUT ON
-- 4(a) Create the prc_insert_appt_serv procedure
CREATE OR REPLACE PROCEDURE prc_insert_appt_serv (
    p_appt_no IN NUMBER,
    p_service_code IN CHAR,
    p_output OUT VARCHAR2
) AS
    v_provider_code CHAR(6);
    v_service_provider CHAR(4);
    v_count NUMBER;
BEGIN
    -- Check if appointment number is valid
    SELECT COUNT(*) INTO v_count
    FROM APPOINTMENT
    WHERE appt_no = p_appt_no;

    IF v_count = 0 THEN
        p_output := 'Invalid Appointment Number';
    RETURN;
    END IF;
    -- Check if service code is valid
    SELECT COUNT(*) INTO v_count
    FROM PROVIDER_SERVICE
    WHERE service_code = p_service_code;

    IF v_count = 0 THEN
        p_output := 'Invalid Service Code';
    RETURN;
    END IF;

    -- Retrieve the provider code for the given appointment number and service code
    SELECT provider_code INTO v_provider_code
    FROM APPOINTMENT
    WHERE appt_no = p_appt_no;
    
    SELECT COUNT(*) INTO v_count
FROM PROVIDER_SERVICE
WHERE service_code = p_service_code AND provider_code = v_provider_code;

IF v_count = 0 THEN
    p_output := 'Provider is not able to provide the service for this appointment';
    RETURN;
ELSE
    INSERT INTO APPT_SERV(appt_no, service_code)
    VALUES (p_appt_no, p_service_code);
    p_output := 'Insertion Successful';
END IF;
END;
/

-- Test Harness for 4(a)

-- Test 1 - when the service code doesn't exist
DECLARE
    v_output VARCHAR2(100);
BEGIN
    prc_insert_appt_serv(3, 'X003', v_output);
    DBMS_OUTPUT.PUT_LINE('Test 1: ' || v_output);
END;
/

-- Test 2 - when the appointment number doesn't exist
DECLARE
    v_output VARCHAR2(100);
BEGIN
    prc_insert_appt_serv(123, 'RC03', v_output);
    DBMS_OUTPUT.PUT_LINE('Test 2: ' || v_output);
END;
/

-- Test 3 - if the provider is not able to provide that service
DECLARE
    v_output VARCHAR2(4000);
BEGIN
    prc_insert_appt_serv(5, 'RC04', v_output);
    DBMS_OUTPUT.PUT_LINE('Test 3: ' || v_output);
END;
/

-- Test 4 - if it's inserted successfully
DECLARE
    v_output VARCHAR2(4000);
BEGIN
    prc_insert_appt_serv(5, 'P001', v_output);
    DBMS_OUTPUT.PUT_LINE('Test 4: ' || v_output);
END;
/

-- 4b
create or replace trigger trg_apptservice_item_changes
before insert or update or delete on APPTSERVICE_ITEM
for each row
declare
    v_item_exists number;
    v_service_exists number;
    v_appt_exists number;
    v_item_cost number;
    v_item_count number;
    v_continue_execution number := 1; -- this flag to help control the flow
begin
    -- Determine the ITEM_ID based on the operation
    IF inserting OR updating THEN
        SELECT ITEM_STDCOST INTO v_item_cost FROM ITEM WHERE ITEM_ID = :new.ITEM_ID;
    ELSE
        SELECT ITEM_STDCOST INTO v_item_cost FROM ITEM WHERE ITEM_ID = :old.ITEM_ID;
    END IF;

    -- For Insert and Update
    if inserting or updating then
        -- Check if APPT_NO and SERVICE_CODE exists in APPT_SERV
        select count(*) into v_service_exists from APPT_SERV where APPT_NO = :new.APPT_NO and SERVICE_CODE = :new.SERVICE_CODE;
        if v_service_exists = 0 then
            raise_application_error(-20002, 'Error: APPT_NO and SERVICE_CODE combination does not exist.');
            v_continue_execution := 0;
        end if;

        -- Check if ITEM_ID exists in ITEM table, only if the previous step was successful
        if v_continue_execution = 1 then
            select count(*) into v_item_exists from ITEM where ITEM_ID = :new.ITEM_ID;
            if v_item_exists = 0 then
                raise_application_error(-20003, 'Error: ITEM_ID does not exist.');
                v_continue_execution := 0;
            end if;
        end if;

        -- If all checks passed, perform the updates
        if v_continue_execution = 1 then
            if updating then
                -- Revert the old stock deduction and apply the new deduction
                update ITEM
                set ITEM_STOCK = ITEM_STOCK + :old.AS_ITEM_QUANTITY - :new.AS_ITEM_QUANTITY
                where ITEM_ID = :new.ITEM_ID;
                
                -- Updating APPTSERV_ITEMCOST in APPT_SERV table
                update APPT_SERV
                set APPTSERV_ITEMCOST = NVL(APPTSERV_ITEMCOST, 0) - (v_item_cost * :old.AS_ITEM_QUANTITY) + (v_item_cost * :new.AS_ITEM_QUANTITY)
                where APPT_NO = :new.APPT_NO and SERVICE_CODE = :new.SERVICE_CODE;
            else
                -- Logic for the insert operation
                update ITEM
                set ITEM_STOCK = ITEM_STOCK - :new.AS_ITEM_QUANTITY
                where ITEM_ID = :new.ITEM_ID;

                -- Updating APPTSERV_ITEMCOST in APPT_SERV table
                update APPT_SERV
                set APPTSERV_ITEMCOST = NVL(APPTSERV_ITEMCOST, 0) + (v_item_cost * :new.AS_ITEM_QUANTITY)
                where APPT_NO = :new.APPT_NO and SERVICE_CODE = :new.SERVICE_CODE;
            end if;
        end if;
    end if;

    -- For Delete
    IF deleting THEN
        -- Check for ITEM_ID existence in ITEM table without using EXCEPTION
        SELECT COUNT(*) INTO v_item_count FROM ITEM WHERE ITEM_ID = :old.ITEM_ID;
        IF v_item_count = 0 THEN
            raise_application_error(-20004, 'Error: ITEM_ID from deleted record does not exist in ITEM table.');
        RETURN;
        ELSE
            -- Update ITEM_STOCK in ITEM table
            update ITEM
            set ITEM_STOCK = ITEM_STOCK + :old.AS_ITEM_QUANTITY
            where ITEM_ID = :old.ITEM_ID;

            -- Revert changes to APPTSERV_ITEMCOST in APPT_SERV table
            update APPT_SERV
            set APPTSERV_ITEMCOST = NVL(APPTSERV_ITEMCOST, 0) - (v_item_cost * :old.AS_ITEM_QUANTITY)
            where APPT_NO = :old.APPT_NO and SERVICE_CODE = :old.SERVICE_CODE;
        END IF;
    end if;
    dbms_output.put_line ('ITEM_STOCK and APPTSERV_ITEMCOST values have been updated');
end;
/
-- Test Harness for 4(b)
-- Display the original values
DECLARE
    v_original_stock NUMBER;
    v_original_cost NUMBER;
BEGIN
    SELECT ITEM_STOCK INTO v_original_stock FROM ITEM WHERE ITEM_ID = 1;
    SELECT APPTSERV_ITEMCOST INTO v_original_cost FROM APPT_SERV WHERE APPT_NO = 5 AND SERVICE_CODE = 'P001';
    DBMS_OUTPUT.PUT_LINE('Original ITEM_STOCK: ' || v_original_stock);
    DBMS_OUTPUT.PUT_LINE('Original APPTSERV_ITEMCOST: ' || v_original_cost);
END;
/

-- Insert a new row into APPTSERVICE_ITEM
INSERT INTO APPTSERVICE_ITEM (AS_ID, APPT_NO, SERVICE_CODE, ITEM_ID, AS_ITEM_QUANTITY) VALUES (1, 5, 'P001', 1, 2);

-- Display values after insertion
DECLARE
    v_post_insert_stock NUMBER;
    v_post_insert_cost NUMBER;
BEGIN
    SELECT ITEM_STOCK INTO v_post_insert_stock FROM ITEM WHERE ITEM_ID = 1;
    SELECT APPTSERV_ITEMCOST INTO v_post_insert_cost FROM APPT_SERV WHERE APPT_NO = 5 AND SERVICE_CODE = 'P001';
    DBMS_OUTPUT.PUT_LINE('Post Insert ITEM_STOCK: ' || v_post_insert_stock);
    DBMS_OUTPUT.PUT_LINE('Post Insert APPTSERV_ITEMCOST: ' || v_post_insert_cost);
END;
/

-- Update the row in APPTSERVICE_ITEM
UPDATE APPTSERVICE_ITEM SET AS_ITEM_QUANTITY = 7 WHERE AS_ID = 1;
-- Display values after update
DECLARE
    v_post_update_stock NUMBER;
    v_post_update_cost NUMBER;
BEGIN
    SELECT ITEM_STOCK INTO v_post_update_stock FROM ITEM WHERE ITEM_ID = 1;
    SELECT APPTSERV_ITEMCOST INTO v_post_update_cost FROM APPT_SERV WHERE APPT_NO = 5 AND SERVICE_CODE = 'P001';
    DBMS_OUTPUT.PUT_LINE('Post Update ITEM_STOCK: ' || v_post_update_stock);
    DBMS_OUTPUT.PUT_LINE('Post Update APPTSERV_ITEMCOST: ' || v_post_update_cost);
END;
/

-- Delete the row from APPTSERVICE_ITEM
DELETE FROM APPTSERVICE_ITEM WHERE AS_ID = 1;
-- Display values after deletion
DECLARE
    v_post_delete_stock NUMBER;
    v_post_delete_cost NUMBER;
BEGIN
    SELECT ITEM_STOCK INTO v_post_delete_stock FROM ITEM WHERE ITEM_ID = 1;
    SELECT APPTSERV_ITEMCOST INTO v_post_delete_cost FROM APPT_SERV WHERE APPT_NO = 5 AND SERVICE_CODE = 'P001';
    DBMS_OUTPUT.PUT_LINE('Post Delete ITEM_STOCK: ' || v_post_delete_stock);
    DBMS_OUTPUT.PUT_LINE('Post Delete APPTSERV_ITEMCOST: ' || v_post_delete_cost);
END;
/