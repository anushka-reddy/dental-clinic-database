# Monash New Smile (MNS) Database

This repository contains the solutions to a few tasks as per the business requirements for the Monash New Smile (MNS) database with a focus on database design and SQL queries related to managing dental services, emergency contacts, patients, items, appointments, providers, provider's specialisation, and nurses.

## 2A Overview

TASK 1: DDL (Data Definition Language):
Manually adding CREATE TABLE and CONSTRAINT definitions for the EMERGENCY_CONTACT, PATIENT, and APPOINTMENT tables, following specific instructions and maintaining identical table and attribute names as provided in the data model.

TASK 2: Populate Sample Data:
Inserting mock data into the database to simulate real-world scenarios and facilitate testing or demonstration purposes, ensuring that the data adheres to specific criteria and rules outlined in the task description.

TASK 3: DML (Data Manipulation Language):
Perform operations like INSERT, UPDATE, DELETE to manipulate data stored in the database tables, primarily recording appointments for patients and emergency contacts, scheduling follow-up appointments, rescheduling appointments, and cancelling appointments due to provider absence. It also involves creating sequences for primary keys.

TASK 4: DATABASE MODIFICATIONS:
Make alterations to the database schema or structure, including adding or removing columns, changing data types, and altering constraints. Specifically, adding a new attribute to record the total number of appointments for each patient, allowing patients to register multiple emergency contact persons, structuring the database to store nurse training logs, ensuring compatibility with existing data and demonstrating changes with appropriate select and desc statements.

TASK 5: Transaction Theory:
Understand and apply concepts related to database transactions, including ACID properties (Atomicity, Consistency, Isolation, Durability), transaction management, concurrency control, and transaction isolation levels.

## 2B Overview

TASK 1: Relational Database Queries - Relational Algebra:
Perform operations like selection, projection, Cartesian product, join, union, intersection, set difference, and renaming using relational algebra. Specifically, for queries involving item usage in appointments, patient and emergency contact details based on appointment and city, and patients attended by endodontists, demonstrating an understanding of query efficiency.

TASK 2: Relational Database Queries - SQL:
Write SQL queries for selecting, joining, filtering, sorting, grouping, aggregating, sub-querying, updating, deleting, inserting, altering tables, creating/dropping tables, indexing, and applying constraints. Specifically, to retrieve information from the database, including listing items with certain stock levels and descriptions, providers specializing in pediatric dentistry, services with fees higher than the average, appointments with the highest total cost, service fee differentials, follow-up appointment percentages for patients, and appointment-related statistics for providers within a specified period.

TASK 3: Non-Relational Database Queries - MongoDB:
Query MongoDB documents, filter, sort, aggregate data, update/delete documents, insert data, create indexes, and work with MongoDB's unique features like embedded documents and flexible schemas. It specifically involves generating a collection of JSON documents representing appointment details from relational database tables and performing MongoDB commands to create a new collection, list appointments using specific criteria, change item descriptions, and insert additional items into a specific appointment.

TASK 4: PL/SQL:
Write PL/SQL code for stored procedures, functions, control structures, exception handling, triggers, cursors, variable declaration, data types, and executing SQL statements within PL/SQL blocks. It involves writing SQL commands in the supplied SQL script T4-mns-plsql.sql to create a stored procedure for inserting appointment services, handling input arguments and output, and creating a trigger to automatically update item stock and appointment service item costs when APPTSERVICE_ITEM rows are inserted, updated, or deleted, including test harness SQL commands to demonstrate the successful operation of the stored procedure and trigger.

## Instructions

Follow these steps to use the solutions provided in this repository:

1. **Clone the Repository**: Clone this repository to your local machine using the `git clone` command.

2. **Navigate to the Repository**: Move into the repository directory.

3. **Review the Solutions**: Review the SQL scripts provided in the `/2A` or `/2B` folder to understand the solutions for each task.

4. **Execute SQL Scripts**: Execute the SQL scripts connecting to Monash's database management system (DBMS) to apply the solutions to your MNS database.

5. **Verify Results**: Verify the correctness of the applied solutions by querying the database and comparing the results with the expected outcomes.

6. **Feedback and Contributions**: If you have any feedback or improvements to suggest, feel free to open an issue or submit a pull request.

## Acknowledgments

Special thanks to Monash University for providing the materials and database schema.

---
