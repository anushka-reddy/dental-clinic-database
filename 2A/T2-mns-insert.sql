--*****PLEASE ENTER YOUR DETAILS BELOW*****
--T2-mns-insert.sql

--Student ID: 32071582
--Student Name: Anushka Reddy
--Unit Code: FIT3171
--Applied Class No: 04

/* Comments for your marker:
*/

--------------------------------------
--INSERT INTO emergency_contact
--------------------------------------

INSERT INTO EMERGENCY_CONTACT (ec_id, ec_fname, ec_lname, ec_phone) VALUES (
    1, 
    'John', 
    'Doe', 
    '0499578987'
);

INSERT INTO EMERGENCY_CONTACT (ec_id, ec_fname, ec_lname, ec_phone) VALUES (
    2, 
    'Jane', 
    'Smith', 
    '0479878987'
);

INSERT INTO EMERGENCY_CONTACT (ec_id, ec_fname, ec_lname, ec_phone) VALUES (
    3, 
    'Robert', 
    'Brown', 
    '0472965987'
);

INSERT INTO EMERGENCY_CONTACT (ec_id, ec_fname, ec_lname, ec_phone) VALUES (
    4, 
    'Linda', 
    'White', 
    '0472812987'
);

INSERT INTO EMERGENCY_CONTACT (ec_id, ec_fname, ec_lname, ec_phone) VALUES (
    5, 
    'Michael', 
    'Green', 
    '0489578987'
);

--------------------------------------
--INSERT INTO patient
--------------------------------------

-- 5 adult patients
INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    10, 'Alex', 'Turner', '123 Arctic St', 'Melbourne', 'VIC', '3000', 
    TO_DATE('1990-01-01', 'YYYY-MM-DD'), '5678901234', 'alex.turner@email.com', 1
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    11, 'Olivia', 'Cook', '456 Moon Ave', 'Sydney', 'NSW', '2000', 
    TO_DATE('1985-02-03', 'YYYY-MM-DD'), '6789012345', 'olivia.cook@email.com', 2
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    12, 'Ethan', 'Hunt', '789 Star Rd', 'Brisbane', 'QLD', '4000', 
    TO_DATE('1995-04-05', 'YYYY-MM-DD'), '7890123456', 'ethan.hunt@email.com', 3
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    13, 'Ava', 'Ray', '101 Galaxy Ln', 'Perth', 'WA', '6000', 
    TO_DATE('1987-06-07',  'YYYY-MM-DD'), '8901234567', 'ava.ray@email.com', 4
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    14, 'Lucas', 'Sky', '112 Sun Blvd', 'Adelaide', 'SA', '5000', 
    TO_DATE('1982-08-09',  'YYYY-MM-DD'), '9012345678', 'lucas.sky@email.com', 5
);

-- 5 under-age patients
INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    15, 'Mia', 'Snow', '123 Arctic St', 'Melbourne', 'VIC', '3000', 
    TO_DATE('2010-01-11',  'YYYY-MM-DD'), '0123456789', 'mia.snow@email.com', 3
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    16, 'Noah', 'River', '456 Moon Ave', 'Sydney', 'NSW', '2000', 
    TO_DATE('2008-02-13',  'YYYY-MM-DD'), '1234567890', 'noah.river@email.com', 2
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    17, 'Emma', 'Lake', '789 Star Rd', 'Brisbane', 'QLD', '4000', 
    TO_DATE('2007-04-15',  'YYYY-MM-DD'), '2345678901', 'emma.lake@email.com', 3
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    18, 'Liam', 'Ocean', '101 Galaxy Ln', 'Perth', 'WA', '6000', 
    TO_DATE('2012-06-17',  'YYYY-MM-DD'), '3456789012', 'liam.ocean@email.com', 4
);

INSERT INTO PATIENT (
    patient_no, patient_fname, patient_lname, patient_street, patient_city, 
    patient_state, patient_postcode, patient_dob, patient_contactmobile, 
    patient_contactemail, ec_id
) VALUES (
    19, 'Sophia', 'Sea', '112 Sun Blvd', 'Adelaide', 'SA', '5000', 
    TO_DATE('2006-08-19',  'YYYY-MM-DD'), '4567890123', 'sophia.sea@email.com', 4
);

--------------------------------------
--INSERT INTO appointment
--------------------------------------

-- Appointments scheduled on three specific days: 1 May 2023, 10 May 2023, and 20 May 2023
-- Appointments for 1 May 2023
INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
)   VALUES (
        70, TO_DATE('2023-05-01 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), 51, 'S', 10, 'END001', 1, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    71, TO_DATE('2023-05-01 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), 52, 'T', 11, 'GEN001', 2, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    72, TO_DATE('2023-05-01 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), 53, 'L', 12, 'GEN002', 3, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    73, TO_DATE('2023-05-01 11:00:00', 'YYYY-MM-DD HH24:MI:SS'), 54, 'S', 13, 'ORS001', 4, NULL
);

-- Appointments for 10 May 2023
-- Parallel appointment with appt no.75
INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    74, TO_DATE('2023-05-10 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), 55, 'T', 14, 'PED001', 5, 70
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    75, TO_DATE('2023-05-10 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), 56, 'S', 15, 'GEN003', 6, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    76, TO_DATE('2023-05-10 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), 57, 'L', 16, 'PED002', 7, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    77, TO_DATE('2023-05-10 11:00:00', 'YYYY-MM-DD HH24:MI:SS'), 58, 'S', 17, 'ORT001', 8, 72
);

-- Appointments for 20 May 2023
-- Parallel appointment with appt no.79
INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    78, TO_DATE('2023-05-20 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), 59, 'S', 18, 'AST001', 9, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    79, TO_DATE('2023-05-20 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), 60, 'T', 19, 'AST002', 10, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    80, TO_DATE('2023-05-20 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), 61, 'L', 10, 'PER001', 11, 74
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    81, TO_DATE('2023-05-20 11:00:00', 'YYYY-MM-DD HH24:MI:SS'), 62, 'S', 11, 'PER002', 12, NULL
);

-- Parallel appointment with appt no.83
INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    82, TO_DATE('2023-05-20 12:00:00', 'YYYY-MM-DD HH24:MI:SS'), 63, 'T', 12, 'PRO001', 13, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    83, TO_DATE('2023-05-20 12:00:00', 'YYYY-MM-DD HH24:MI:SS'), 64, 'L', 13, 'GEN001', 14, NULL
);

INSERT INTO APPOINTMENT (
    appt_no, appt_datetime, appt_roomno, appt_length, patient_no, provider_code,
    nurse_no, appt_prior_apptno
) VALUES (
    84, TO_DATE('2023-05-20 13:00:00', 'YYYY-MM-DD HH24:MI:SS'), 65, 'S', 14, 'GEN002', 15, NULL
);

COMMIT;