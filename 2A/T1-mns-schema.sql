--*****PLEASE ENTER YOUR DETAILS BELOW*****
--T1-mns-schema.sql

--Student ID: 32071582
--Student Name: Anushka Reddy
--Unit Code: FIT3171
--Applied Class No: 04

/* Comments for your marker:
Assuming, by default Oracle will restrict deletes that violate foreign key 
constraints, Thankyou!
*/

-- Task 1 Add Create table statements for the white TABLES below
-- Ensure all column comments, and constraint (other than FK's)
-- are included. FK constraint are to be added at the end of this script

-- TABLE: APPOINTMENT
CREATE TABLE APPOINTMENT (
    appt_no NUMBER(7) NOT NULL,
    appt_datetime DATE NOT NULL,
    appt_roomno NUMBER(2) NOT NULL,
    appt_length CHAR(1) CHECK (appt_length IN ('S', 'T', 'L')) NOT NULL,
    patient_no NUMBER(4) NOT NULL,
    provider_code CHAR(6) NOT NULL,
    nurse_no NUMBER(3) NOT NULL,
    appt_prior_apptno NUMBER(7) UNIQUE,
    CONSTRAINT appt_pk PRIMARY KEY (appt_no)
);


-- TABLE: EMERGENCY_CONTACT
CREATE TABLE EMERGENCY_CONTACT (
    ec_id NUMBER(4) NOT NULL,
    ec_fname VARCHAR2(30),
    ec_lname VARCHAR2(30),
    ec_phone CHAR(10) NOT NULL UNIQUE,
    CONSTRAINT ec_pk PRIMARY KEY (ec_id)
);


-- TABLE: PATIENT
CREATE TABLE PATIENT (
    patient_no NUMBER(4) NOT NULL,
    patient_fname VARCHAR2(30),
    patient_lname VARCHAR2(30),
    patient_street VARCHAR2(50) NOT NULL,
    patient_city VARCHAR2(20) NOT NULL,
    patient_state VARCHAR2(3) CHECK (patient_state IN ('NT', 'QLD', 'NSW', 'ACT', 'VIC', 'TAS', 'SA', 'WA')) NOT NULL,
    patient_postcode CHAR(4) NOT NULL,
    patient_dob DATE NOT NULL,
    patient_contactmobile CHAR(10) NOT NULL,
    patient_contactemail VARCHAR2(25) NOT NULL,
    ec_id NUMBER(4) NOT NULL,
    CONSTRAINT patient_pk PRIMARY KEY (patient_no)
);

--Ensure no double booking for patients:
ALTER TABLE APPOINTMENT
ADD CONSTRAINT unique_patient_datetime UNIQUE (patient_no, appt_datetime);

--Ensure no double booking for appointment rooms:
ALTER TABLE APPOINTMENT
ADD CONSTRAINT unique_room_datetime UNIQUE (appt_roomno, appt_datetime);

--Ensure no double booking for providers:
ALTER TABLE APPOINTMENT
ADD CONSTRAINT unique_provider_datetime UNIQUE (provider_code, appt_datetime);

--Ensure no double booking for nurses:
ALTER TABLE APPOINTMENT
ADD CONSTRAINT unique_nurse_datetime UNIQUE (nurse_no, appt_datetime);

-- Add all missing FK Constraints below here
ALTER TABLE PATIENT
ADD CONSTRAINT FK_PATIENT_EC
FOREIGN KEY (ec_id) REFERENCES EMERGENCY_CONTACT(ec_id);

ALTER TABLE APPOINTMENT
ADD CONSTRAINT FK_APPOINTMENT_PATIENT
FOREIGN KEY (patient_no) REFERENCES PATIENT(patient_no);

ALTER TABLE APPOINTMENT
ADD CONSTRAINT FK_APPOINTMENT_PROVIDER
FOREIGN KEY (provider_code) REFERENCES PROVIDER(provider_code);

ALTER TABLE APPOINTMENT
ADD CONSTRAINT FK_APPOINTMENT_NURSE
FOREIGN KEY (nurse_no) REFERENCES NURSE(nurse_no);

ALTER TABLE APPOINTMENT
ADD CONSTRAINT FK_APPOINTMENT_PRIOR_APPT
FOREIGN KEY (appt_prior_apptno) REFERENCES APPOINTMENT(appt_no);

COMMIT;