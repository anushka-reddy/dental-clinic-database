--*****PLEASE ENTER YOUR DETAILS BELOW*****
--T3-mns-dm.sql

--Student ID: 32071582
--Student Name: Anushka Reddy
--Unit Code: FIT3171
--Applied Class No: 04

/* Comments for your marker:




*/


--3(a)
-- For EMERGENCY_CONTACT
SELECT sequence_name FROM user_sequences WHERE sequence_name = 'EMERGENCY_CONTACT_SEQ';
DROP SEQUENCE EMERGENCY_CONTACT_SEQ;
CREATE SEQUENCE EMERGENCY_CONTACT_SEQ START WITH 100 INCREMENT BY 5;

-- For PATIENT
SELECT sequence_name FROM user_sequences WHERE sequence_name = 'PATIENT_SEQ';
DROP SEQUENCE PATIENT_SEQ;
CREATE SEQUENCE PATIENT_SEQ START WITH 100 INCREMENT BY 5;

-- For APPOINTMENT
SELECT sequence_name FROM user_sequences WHERE sequence_name = 'APPOINTMENT_SEQ';
DROP SEQUENCE APPOINTMENT_SEQ;
CREATE SEQUENCE APPOINTMENT_SEQ START WITH 100 INCREMENT BY 5;

--3(b)
-- Register Jonathan Robey as an emergency contact.
INSERT INTO EMERGENCY_CONTACT
VALUES (EMERGENCY_CONTACT_SEQ.NEXTVAL, 'Jonathan', 'Robey', '0412523122');

-- Register Laura and Lachlan as patients with Jonathan as their emergency contact.
INSERT INTO PATIENT
VALUES (PATIENT_SEQ.NEXTVAL, 'Laura', 'Robey', '42 King Street', 'Melbourne', 
'VIC', '3000', TO_DATE('2009-01-01','YYYY-MM-DD'), '0400123456', 
'laura.robey@email.com', (SELECT ec_id FROM EMERGENCY_CONTACT 
WHERE ec_phone = '0412523122'));

INSERT INTO PATIENT
VALUES (PATIENT_SEQ.NEXTVAL, 'Lachlan', 'Robey', '42 King Street', 'Melbourne', 
'VIC', '3000', TO_DATE('2011-01-01','YYYY-MM-DD'), '0440148659', 
'lachlan.robey@email.com', (SELECT ec_id FROM EMERGENCY_CONTACT 
WHERE ec_phone = '0412523122'));

-- Create two appointments for Laura and Lachlan.
INSERT INTO APPOINTMENT
VALUES (APPOINTMENT_SEQ.NEXTVAL, TO_DATE('2023-09-04 15:30:00', 
'YYYY-MM-DD HH24:MI:SS'), 66, 'S', (SELECT patient_no FROM patient WHERE
patient_fname = 'Laura'), (SELECT provider_code FROM provider WHERE
provider_title = 'Dr' AND provider_fname = 'Bruce' AND UPPER
(provider_lname) = 'STRIPLIN'), (SELECT nurse_no FROM nurse WHERE 
nurse_fname = 'Chelsea' AND nurse_lname = 'Ford'), NULL);

INSERT INTO APPOINTMENT
VALUES (APPOINTMENT_SEQ.NEXTVAL, TO_DATE('2023-09-04 16:00:00', 
'YYYY-MM-DD HH24:MI:SS'), 67, 'S', (SELECT patient_no FROM patient WHERE
patient_fname = 'Lachlan'), (SELECT provider_code FROM provider WHERE 
provider_title = 'Dr' AND provider_fname = 'Bruce' AND  UPPER (provider_lname) = 
'STRIPLIN'), (SELECT nurse_no FROM nurse WHERE nurse_fname = 'Chelsea' AND
nurse_lname = 'Ford'), NULL);

--3(c)
INSERT INTO APPOINTMENT
VALUES (
    APPOINTMENT_SEQ.NEXTVAL, 
    TO_DATE((SELECT TO_CHAR(appt_datetime + 10, 'YYYY-MM-DD') 
             FROM appointment 
             WHERE TO_DATE(appt_datetime) = TO_DATE('2023-09-04', 'YYYY-MM-DD') 
             AND patient_no = (SELECT patient_no 
                               FROM patient 
                               WHERE patient_fname = 'Lachlan' AND patient_lname = 'Robey') 
             AND provider_code = (SELECT provider_code 
                                  FROM provider 
                                  WHERE provider_title = 'Dr' 
                                  AND provider_fname = 'Bruce' 
                                  AND UPPER(provider_lname) = 'STRIPLIN')) || ' 16:00:00', 
    'YYYY-MM-DD HH24:MI:SS'), 
    67, 
    'L', 
    (SELECT patient_no FROM patient WHERE patient_fname = 'Lachlan'), 
    (SELECT provider_code FROM provider WHERE provider_title = 'Dr' AND
            provider_fname = 'Bruce' AND UPPER(provider_lname) = 'STRIPLIN'), 
    (SELECT nurse_no FROM nurse WHERE nurse_fname = 'Katie'), 
    (SELECT appt_no 
     FROM appointment 
     WHERE TO_DATE(appt_datetime) = TO_DATE('2023-09-04', 'YYYY-MM-DD') 
     AND patient_no = (SELECT patient_no 
                       FROM PATIENT 
                       WHERE patient_fname = 'Lachlan' AND patient_lname = 'Robey') 
     AND provider_code = (SELECT provider_code 
                          FROM provider 
                          WHERE provider_title = 'Dr' 
                          AND provider_fname = 'Bruce' 
                          AND UPPER(provider_lname) = 'STRIPLIN'))
);

--3(d)
UPDATE APPOINTMENT
SET appt_datetime = appt_datetime + 4
WHERE appt_datetime = TO_DATE('2023-09-14 16:00:00', 'YYYY-MM-DD HH24:MI:SS')
AND patient_no = (SELECT patient_no FROM PATIENT WHERE patient_fname = 'Lachlan'
AND patient_lname = 'Robey') 
AND provider_code = (SELECT provider_code FROM provider WHERE provider_title =
'Dr' AND provider_fname = 'Bruce' AND UPPER(provider_lname) = 'STRIPLIN');

--3(e)
DELETE FROM APPOINTMENT
WHERE provider_code = (SELECT provider_code FROM provider WHERE provider_title = 'Dr' AND provider_fname = 'Bruce' AND UPPER(provider_lname) = 'STRIPLIN')
AND appt_datetime BETWEEN TO_DATE('2023-09-15', 'YYYY-MM-DD') AND TO_DATE('2023-09-22', 'YYYY-MM-DD');

COMMIT;