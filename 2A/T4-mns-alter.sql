--*****PLEASE ENTER YOUR DETAILS BELOW*****
--T4-mns-alter.sql

--Student ID: 32071582
--Student Name: Anushka Reddy
--Unit Code: FIT3171
--Applied Class No: 04

/* Comments for your marker:
4b) Adding another table to have a more normalized database. I have deleted the
ec_id column in the patient to avoid redundancy and the use of foreign keys in
this junction table that ensures data integrity.
4c) I have introduced a separate NURSE_TRAINING_LOG table to handle the
many-to-many training relationship between nurses, ensuring data integrity 
without redundancy. I believe, it will provide flexibility for future updates or
changes related to nurse training without impacting other database components.
Thankyou!
*/

--4(a)
ALTER TABLE PATIENT 
ADD total_appointments NUMBER(4);

-- Update the new column with the current count of appointments for each patient
UPDATE PATIENT p 
SET total_appointments = (
    SELECT COUNT(appt_no) 
    FROM APPOINTMENT a 
    WHERE a.patient_no = p.patient_no
);

DESC PATIENT;

SELECT patient_no, patient_fname, patient_lname, total_appointments FROM PATIENT;


--4(b)
DROP TABLE PATIENT_EC_RELATION CASCADE CONSTRAINTS;

-- TABLE: PATIENT_EC_RELATION
CREATE TABLE PATIENT_EC_RELATION (
    patient_no NUMBER(4) NOT NULL,
    ec_id NUMBER(4) NOT NULL,
    CONSTRAINT per_patient_fk FOREIGN KEY (patient_no) REFERENCES PATIENT(patient_no),
    CONSTRAINT per_ec_fk FOREIGN KEY (ec_id) REFERENCES EMERGENCY_CONTACT(ec_id),
    CONSTRAINT per_pk PRIMARY KEY (patient_no, ec_id)
);

INSERT INTO PATIENT_EC_RELATION(patient_no, ec_id)
SELECT patient_no, ec_id FROM PATIENT;

ALTER TABLE PATIENT DROP COLUMN ec_id;

DESC PATIENT;

DESC PATIENT_EC_RELATION;

SELECT * FROM PATIENT_EC_RELATION;

--4(c)
DROP TABLE NURSE_TRAINING_LOG CASCADE CONSTRAINTS;

-- A sequence for training_id
DROP SEQUENCE NTL_training_id_seq;
CREATE SEQUENCE NTL_training_id_seq START WITH 1 INCREMENT BY 1;

-- TABLE: NURSE_TRAINING_LOG
CREATE TABLE NURSE_TRAINING_LOG (
    training_id NUMBER(4) NOT NULL,
    trainee_nurse_no NUMBER(3) NOT NULL,
    trainer_nurse_no NUMBER(3) NOT NULL,
    start_datetime DATE NOT NULL,
    end_datetime DATE NOT NULL,
    training_description VARCHAR2(255) NOT NULL,
    CONSTRAINT ntl_pk PRIMARY KEY (training_id),
    CONSTRAINT ntl_trainee_fk FOREIGN KEY (trainee_nurse_no) REFERENCES nurse(nurse_no),
    CONSTRAINT ntl_trainer_fk FOREIGN KEY (trainer_nurse_no) REFERENCES nurse(nurse_no)
);

DESC NURSE_TRAINING_LOG;

COMMIT;